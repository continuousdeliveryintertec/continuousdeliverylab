## Contributing to Tutorials ##

Thank you so much for your interest in contributing to this lab.

Just a few quick things to be aware of before you get started.

We welcome issues and pull requests for either adding a new tutorial, or fixing a problem with an existing tutorial. This is a repository for tutorials that showcase Continuous Delivery projects. So if there�s a Continuous Delivery tool for what you�re describing, please use that.

Anything you contribute will be under an Apache license. The Continuous Delivery Committee will choose which tutorials to accept and reject, and will be able to take any tutorials here and use it for any workshop. Likewise, anything posted here may be forked by anyone on Intertec.

For more info please contact richard.simoes@intertecintl.com