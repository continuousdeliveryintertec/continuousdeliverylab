# Continuous Delivery Tutorials and Labs

This repo contains Continuous Delivery labs and tutorials authored by members of the community. We welcome contributions and want to grow the repo.

### About Continuous Delivery
- [Continuous Delivery](http://martinfowler.com/bliki/ContinuousDelivery.html)
- [Deployment Pipeline](http://martinfowler.com/bliki/DeploymentPipeline.html)
- [Adopting Continuous Delivery](https://www.infoq.com/presentations/Adopting-Continuous-Delivery)
- [PuppetConf 2013 Video](https://www.youtube.com/watch?v=C3BUZTxL7Xc)

#### Continuous Delivery tutorials:
* [Continuous delivery demo using Jenkins on DC/OS](cd-demo/)
* [Zero Downtime Deployments Lab](zdd-lab/)

#### Contributing
We want to see this repo grow, so if you have a tutorial to submit please see this guide:

[Guide to submitting your own tutorial](contribute.md)